import re           ###two diff methods to search for something in reg = search and match. Match returns an array on values found in the pattern
                    ###search is used more

##open file##
peopleFH=open("data.txt","r")
ptn=re.compile("^.*ati. ", re.IGNORECASE) #dont care if its uper/lower case

for person in peopleFH:
    #if re.search("22",person):

    if ptn.search(person):
        print(person.rstrip("\r\n"))

peopleFH.seek(0,0)

for person in peopleFH:
###I want to create a variable called matched, store what you find in it
    ###matched=re.search("^.*ati. ",person)
    matched=ptn.search(person)

    if matched:
        print("Matched at",matched.span())     #span puts it on a new line
        print("Words found:",matched.group())   #group shows what it found
