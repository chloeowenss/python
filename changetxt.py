import re

ptn=re.compile("\s+")
fh=open("newdata.txt", "r")

# Remove lots of spaces to single space

for line in fh:
    data=ptn.split(line)        #split the line into spaces
    print(" ".join(data))
fh.seek(0,0)

print("Round again")
ptn2=re.compile("#.*")

for line in fh:     #check for a # at the beginning or partly through

    #remove lines that begin with a#
    if re.search('^#',line):
        continue

    if re.search('#',line):
        line=ptn2.sub("",line)
    print(line.rstrip("\r\n"))





fh.close()
