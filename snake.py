# Python 2
#print "Hello World"

import sys

#Python 3
print("Hello world")

myname=input("Enter your name:")

print("Hello "+myname)  ###print forces a new line

print("Command line args: "+str(sys.argv))
print(sys.argv[1])


#Using files

#Regular expressions

#Loops

#Conditions

#####FOR LOOP######
peopleFH=open("data.txt","r")
for person in peopleFH:
    ## sys.stdout.write(person)    ##prints exactly like file
    ## print(person.rstrip('\r\n'))
    fields=person.split(',')
    fields[-1]=fields[-1].rstrip("\r\n")
    #print("Number of elements: "+str(len(fields))+"\tLastElement is: "+str(len(fields)-1))
    # print("Name: "+fields[0]+"\t"+"Phone: "+fields[-1])

##check the data is in there
    if fields[1] =="22":         #""makes it string based, it must be string based"
        print(type(fields[1]))  #determine the type of data stored in variable
        print("New age: "+str(int(fields[1])+1))
        print("Name: "+fields[0]+"\t"+"Phone: "+fields[len(fields)-1])

peopleFH.close()
