import re

ptn2=re.compile("#.*")
macptn=re.compile("([a-fA-F0-9]{2}[:|\-]?){6}")
ptn=re.compile("\s+")
ipptn=re.compile("(([2][5][0-5]\.)|([2][0-4][0-9]\.)|([0-1]?[0-9]?[0-9]\.)){3}(([2][5][0-5])|([2][0-4][0-9])|([0-1]?[0-9]?[0-9]))")

fh=open("hosts.real", "r")
outfh=open("temp.txt", "w")
newfh=open("newfile.txt", "w")

fh.readline()

#Remove all comments from the somefile
for line in fh:     #check for a # at the beginning or partly through

    #remove lines that begin with #


    if re.search('^#',line):
        continue

    if re.search('#',line):
        line=ptn2.sub("",line)
    data=ptn.split(line)
    outfh.write(" ".join(data)+"\n")

#Extract all MAC addresses from file

    macad=macptn.search(line)
    if macad:
        print(macad.group())

#Remove all extra spaces and write IP addresses to a new file

    ipad=ipptn.search(line)
    if ipad:
        print(ipad.group())
    newfh.write(data[0]+"\n")

print("newfile.txt has been created")
outfh.close()
fh.close()
newfh.close()
