#grab text of a particular format
import re
phoneptn=re.compile(",[0-9]{3,}-[0-9]{4,},")

#postcode eg
#pcde=re.compile("[a-zA-Z]{2}[0-9{1,}[a-zA-Z]? ?[0-9][a-zA-Z]{2,}]")


peopleFH=open("data.txt", "r")
outpeopleFH=open("phones.txt", "w")
peopleFH.readline()     ###read first line

for line in peopleFH:               ###read all lines
    # fixed field position method
    data=line.split(",")
    print(data[2])
    outpeopleFH.write(data[2]+"\n")

print("phones.txt has been created")
outpeopleFH.close()
peopleFH.close()


# phone number could be anywhere on the line
peopleFH=open("data.txt", "r")
outpeopleFH=open("phones.txt", "w")

peopleFH.readline()     ###read first line

for line in peopleFH:
    phonenum=phoneptn.search(line)
    if phonenum:
        print(phonenum.group().replace(",",""))
        outpeopleFH.write(phonenum.group().replace(",","")+"\n")

outpeopleFH.close()
peopleFH.close()
